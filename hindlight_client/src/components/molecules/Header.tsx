import * as React from 'react';
import Box from '../atoms/Box';
import Text from '../atoms/Text';

const Header: React.FC = () => {
  return (
    <Box p={4} bg="black">
      <Text color="blue" fontSize={5}>HindLight</Text>
    </Box>
  );
};

export default Header;
