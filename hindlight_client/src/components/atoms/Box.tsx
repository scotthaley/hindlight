import styled from 'styled-components';
import { color, space, width } from 'styled-system';

const Box = styled.div`
  ${space}
  ${width}
  ${color}
`;

export default Box;
