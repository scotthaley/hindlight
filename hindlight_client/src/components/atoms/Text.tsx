import styled from 'styled-components';
import {
  color,
  fontSize,
  ColorProps,
  FontSizeProps
} from 'styled-system';

interface ITextProps extends ColorProps, FontSizeProps {
}

const Text = styled.span<ITextProps>`
  ${color}
  ${fontSize}
`;

export default Text;
