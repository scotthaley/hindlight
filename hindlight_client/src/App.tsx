import React, { Component } from 'react';
import { ThemeProvider } from 'styled-components';
import './App.css';
import theme from './utils/theme';
import Header from './components/molecules/Header';

class App extends Component {
  render() {
    return (
      <ThemeProvider theme={theme}>
        <div className="App">
          <Header />
        </div>
      </ThemeProvider>
    );
  }
}

export default App;
