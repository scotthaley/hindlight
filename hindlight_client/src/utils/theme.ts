const colors = {
  red: '#E95678',
  lightOrange: '#FAC29A',
  peach: '#F09483',
  blue: '#25B0BC',
  black: '#1C1E26',
  white: '#fff'
}
export default {
  colors
}
