import styled from 'styled-components';
import {
  color,
  space,
  width,
  ColorProps,
  WidthProps,
  SpaceProps
} from 'styled-system';

interface IBoxProps extends ColorProps, WidthProps, SpaceProps {
  flex?: boolean;
}

const Box = styled.div<IBoxProps>`
  ${space}
  ${width}
  ${color}
`;

export default Box;
