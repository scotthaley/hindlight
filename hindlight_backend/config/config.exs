# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :hindlight_backend,
  ecto_repos: [HindlightBackend.Repo]

# Configures the endpoint
config :hindlight_backend, HindlightBackendWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "pmsNLvumEwu8DFSPHVO4aQRB1GQZ0QJP9aMs4g6sJ2p04dktac7QjoSLeMp1pmab",
  render_errors: [view: HindlightBackendWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: HindlightBackend.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
