defmodule HindlightBackend.Repo do
  use Ecto.Repo,
    otp_app: :hindlight_backend,
    adapter: Ecto.Adapters.Postgres
end
